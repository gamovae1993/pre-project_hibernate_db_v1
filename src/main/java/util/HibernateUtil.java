package util;

import model.User;
import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class HibernateUtil {

    private static final String DB_DRIVER = "org.postgresql.Driver";
    private static final String DB_CONNECTION = "jdbc:postgresql://localhost:5432/postgres";
    private static final String DB_USER = "postgres";
    private static final String DB_PASSWORD = "000000";

    private static SessionFactory sessionFactory;
    private static final ConcurrentMap<String, SessionFactory> SESSION_FACTORIES = new ConcurrentHashMap<>();

    private static SessionFactory buildSessionFactory(String configFile) {
        SessionFactory sessionFactory = SESSION_FACTORIES.get(configFile);

        if (sessionFactory == null) {
            try (InputStream inputStream = HibernateUtil.class.getClassLoader().getResourceAsStream(configFile)) {
                Properties properties = new Properties();
                properties.load(inputStream);

                StandardServiceRegistry registry = new StandardServiceRegistryBuilder()
                        .applySettings(properties)
                        .build();

                Metadata metadata = new MetadataSources(registry)
                        .addAnnotatedClass(User.class)
                        .getMetadataBuilder()
                        .build();

                sessionFactory = metadata.getSessionFactoryBuilder().build();
                SESSION_FACTORIES.putIfAbsent(configFile, sessionFactory);

            } catch (IOException e) {
                throw new RuntimeException("Unable to load configuration file: " + configFile, e);
            }
        }
        return sessionFactory;
    }

    public static SessionFactory getSessionFactory() {
        return buildSessionFactory("hibernate.cfg.xml");
    }

    public static SessionFactory getSessionFactory(String configFile) {
        return buildSessionFactory(configFile);
    }

    public static SessionFactory getSessionFactoryJavaTest() {
        if (sessionFactory == null) {
            try {
                Properties prop = new Properties();
                prop.setProperty("hibernate.connection.url", DB_CONNECTION);
                prop.setProperty("hibernate.dialect", "org.hibernate.dialect.PostgreSQLDialect");
                prop.setProperty("hibernate.connection.username", DB_USER);
                prop.setProperty("hibernate.connection.password", DB_PASSWORD);
                prop.setProperty("hibernate.connection.driver_class", DB_DRIVER);
                Configuration configuration = new Configuration().addProperties(prop);
                configuration.addAnnotatedClass(User.class);
                sessionFactory = configuration.buildSessionFactory(
                        new StandardServiceRegistryBuilder()
                                .applySettings(prop)
                                .build()
                );
            } catch (Exception e) {
                System.out.println("Exception!" + e);
            }
        }
        return sessionFactory;
    }
}